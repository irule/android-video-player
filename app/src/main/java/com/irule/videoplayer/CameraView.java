package com.irule.videoplayer;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.RelativeLayout;

import tcking.github.com.giraffeplayer.IjkVideoView;

public class CameraView extends AppCompatActivity
{

    private static final String TAG = "CAMERA_VIEW";
    private static final String TEST_CAMERA_URI_1 = "https://archive.org/download/BigBuckBunny_328/BigBuckBunny_512kb.mp4";
    private static final String TEST_CAMERA_URI_2 = "rtsp://205.157.152.230/axis-media/media.amp?videocodec=h264";

    @Override
    protected void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_view);

        IjkVideoView videoView = (IjkVideoView) findViewById(R.id.video_view_1);
        setVideoSource(videoView, TEST_CAMERA_URI_1);
        videoView.start();

        videoView = new IjkVideoView(this);
        setVideoSource(videoView, TEST_CAMERA_URI_2);
        videoView.start();

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.video_view_2);
        layout.addView(videoView);
    }

    public void setVideoSource(IjkVideoView videoView, String videoSource)
    {
        Uri mVideoUri = Uri.parse(videoSource);

        if (mVideoUri != null)
        {
            videoView.setVideoURI(mVideoUri);
        }
        else
        {
            Log.e(TAG, "Null Data Source\n");
        }
    }
}
